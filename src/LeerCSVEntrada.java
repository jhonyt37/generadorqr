
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author jmtoro
 */
public class LeerCSVEntrada {
    
    private static String replaceSpecialChars(String in){
        String res="";
        String dataError ="";
        System.out.println( res = in.replaceAll("[^\\dA-Za-z]", ""));
        return res;
    }

    public static float countLinesFile(String rutaArchivo) {
        float fileSize = 0;
        try {
            BufferedReader br;
            
            br = new BufferedReader(new FileReader(rutaArchivo));
            while ((br.readLine()) != null) {
                fileSize++;
            }

        } catch (IOException ex) {
            Logger.getLogger(LeerCSVEntrada.class.getName()).log(Level.SEVERE, null, ex);
        }
        return fileSize;
    }
    /**
     *
     * @param entrada
     * @param lblSalida
     * @param lblSize
     * @param rd
     * @param out
     */
    public static void LeerCSV( File entrada, JLabel lblSalida,JLabel lblSize,String rd,String out) {
	
        
        String rutaArchivo = "" + entrada.getAbsolutePath();
        String resto = "" + entrada.getName();
        String rutaDestino = rd+"\\";
        String logoQR =  System.getProperty("user.dir") + "/recursos/imagenes/logo_redeban.png";
        
        QRFrame.serverLogger.escribeLogInfo("paso 1 " + rutaArchivo);
        QRFrame.serverLogger.escribeLogInfo("paso 2 " + resto);
        QRFrame.serverLogger.escribeLogInfo("paso 3 " + rutaDestino);

        FicheroSalida salida = null;
        BufferedReader br;
        String line = "";
        String cvsSplitBy = ",";
        lblSalida.setText("Progreso : 0%");
        purgeTmpOut("./recursos/tmp/");
        try {

            float fileSize = countLinesFile(rutaArchivo);
            
            lblSalida.setText("Progreso : 1%");
            lblSize.setText("Registros: "+(fileSize-1));
            QRFrame.serverLogger.escribeLogInfo("Progreso : 1% tamaño de archivo "+fileSize+ " registros");
            
            
            float steps = 100/fileSize;
            QRFrame.serverLogger.escribeLogInfo("filesize = "+fileSize+ " "+steps);
            br = new BufferedReader(new FileReader(rutaArchivo));
            int i = 0, j=0;
            while ((line = br.readLine()) != null) {
                // use comma as separator
                line = line.replace("\"", "");
                String[] lineIn = line.split(cvsSplitBy);
                lineIn[0] = replaceSpecialChars(lineIn[0]);
                int cu=-1;
                try{
                    Integer.parseInt(lineIn[0]);
                    cu =0;
                }
                catch(NumberFormatException ex){
                    QRFrame.serverLogger.escribeLogInfo("registro no procesado "+lineIn[0]);
                }
                        
                if(lineIn[0].equals("Codigo Unico") || lineIn[0].endsWith("ico") || j==0){
                    QRFrame.serverLogger.escribeLogInfo("Archivo de entrada OK");
                    salida = new FicheroSalida((int)fileSize,out,rutaDestino,lblSalida);
                    j++;
                    lblSalida.setText("Progreso : 2%");
                    QRFrame.serverLogger.escribeLogInfo("Progreso : 2%");
                }
                else if(lineIn.length == 4 && cu == 0) {
                    try {
                        File img = UTILSQR.getDataQR(lineIn[3], "./recursos/tmp/out_" + i + ".bmp");

                        String dataRes = UTILSQR.getDecodedDataQR(img.getAbsolutePath());
                        File imgLogo = GeneradorQR.generate(dataRes, logoQR, "./recursos/tmp/outLogo_" + i);
                        

                        List<TLV_Struct> listData = UTILSQR.getTLVData(dataRes);
                        String datos50 = UTILSQR.searchTLVData(UTILSQR.TAG_QR_CU, listData);

                        List<TLV_Struct> list50 = UTILSQR.getTLVData(datos50);
                        String codigoUnico = UTILSQR.searchTLVData(UTILSQR.TAG_QR_50_CU, list50);

                        String datos64 = UTILSQR.searchTLVData(UTILSQR.TAG_QR_CAMPO_64, listData);
                        List<TLV_Struct> list64 = UTILSQR.getTLVData(datos64);

                        String nombreComercio = UTILSQR.searchTLVData(UTILSQR.TAG_QR_64_COMERCIO, list64);

                        String ciudad = UTILSQR.searchTLVData(UTILSQR.TAG_QR_CIUDAD, listData);

                        String tagIva = UTILSQR.searchTLVData(UTILSQR.TAG_QR_IVA, listData);
                        List<TLV_Struct> list82 = UTILSQR.getTLVData(tagIva);
                        String valorIva = UTILSQR.searchTLVData(UTILSQR.TAG_QR_82_VALOR, list82);

                        String data62 = UTILSQR.searchTLVData(UTILSQR.TAG_QR_CAMPO_62, listData);
                        List<TLV_Struct> list62 = UTILSQR.getTLVData(data62);
                        String terminal = UTILSQR.searchTLVData(UTILSQR.TAG_QR_62_TERMINAL, list62);
                        String tOperacion = UTILSQR.searchTLVData(UTILSQR.TAG_QR_62_TIPO_OPERACION, list62);

                        imgLogo = GeneradorQR.addStringsImage(
                                imgLogo,
                                0,
                                imgLogo.getAbsolutePath(),
                                nombreComercio,
                                "Cod. Comercio: " + codigoUnico.replaceFirst("00",""), 
                                " ");
                        imgLogo = GeneradorQR.addStringsImage(
                                imgLogo,
                                1,
                                imgLogo.getAbsolutePath(),
                                "Terminal: "+terminal,
                                "IVA: "+valorIva+"%",
                                " ");
                        
                        salida.setMatrizData(i,
                                codigoUnico,
                                nombreComercio,
                                dataRes,
                                terminal,
                                ciudad,
                                valorIva,
                                lineIn[3],
                                imgLogo,
                                img);
                        i++;
                    } catch (Exception exj) {
                        QRFrame.serverLogger.escribeLogInfo("error en entrada **" + line + " **" + exj.getMessage());
                        registroinvalido("./recursos/tmp/fallidos.txt", line);
                    }
                QRFrame.serverLogger.escribeLogInfo("");
                }
                else{
                    QRFrame.serverLogger.escribeLogInfo("data de entrada invalida entrada "+lineIn.length);
                }
                lblSalida.setText("Progreso : "+(int)(i*steps)+"%");
                QRFrame.serverLogger.escribeLogInfo("Progreso : "+(int)(i*steps)+"%");
            }
            lblSalida.setText("Progreso : 99% total registros procesados: "+i);
            QRFrame.serverLogger.escribeLogInfo("Progreso : 99% total registros procesados: "+i);
            
            salida.crearExcelsalida();
            //purgeTmpOut("./recursos/tmp/");
            lblSalida.setText("Progreso : 100% registros creados: "+i);
            QRFrame.serverLogger.escribeLogInfo("Progreso : 100% registros creados: "+i);
            JOptionPane.showMessageDialog(null, "Proceso finalizado con exito registros creados: "+i);
            abrirSalida(salida.rutaArchivo);

        } catch (Exception e) {
            Logger.getLogger(QRFrame.class.getName()).log(Level.SEVERE, null, e.getMessage());
            QRFrame.serverLogger.escribeLogInfo("error en **" + line +" **"+ e.getMessage());
            registroinvalido("./recursos/tmp/fallidos.txt",line);
            //JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }
    
    public static void registroinvalido(String ruta,String registro){
         try {
            
            File file = new File(ruta);
            // Si el archivo no existe es creado
            if (!file.exists()) {
                file.createNewFile();
            }
            FileWriter fw = new FileWriter(file,true);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(registro+"\n");
            bw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static void purgeTmpOut(String direccion){
            
        File directorio = new File(direccion);
        File f;
        if (directorio.isDirectory()) {
            String[] files = directorio.list();
            if (files.length > 0) {
                System.out.println(" Directorio vacio: " + direccion);
                for (String archivo : files) {
                    System.out.println(archivo);
                    f = new File(direccion + File.separator + archivo);

                    QRFrame.serverLogger.escribeLogInfo("Ultima modificación: " + new Date(f.lastModified()));
                    long Time;
                    Time = (System.currentTimeMillis() - f.lastModified());
                    long cantidadDia = (Time / 86400000);
                    QRFrame.serverLogger.escribeLogInfo("Age of the file is: " + cantidadDia + " days");

                        QRFrame.serverLogger.escribeLogInfo("Borrado:" + archivo);
                        f.delete();
                        f.deleteOnExit();

                }
            }
        }
    }
    
    public static void abrirSalida(String ruta){
        try{
          Runtime.getRuntime().exec("cmd "+
                  "/c start "
                  + ruta);
          }catch(IOException  e){
              QRFrame.serverLogger.escribeLogInfo("Borrado:" + e.getMessage());
          }
    }
        
}
