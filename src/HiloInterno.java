
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import static javax.swing.SwingConstants.CENTER;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author jmtoro
 */
public class HiloInterno extends Thread{

    /**
     * @return the labelQR
     */
    public JLabel getLabelQR() {
        return labelQR;
    }

    /**
     * @param labelQR the labelQR to set
     */
    public void setLabelQR(JLabel labelQR) {
        this.labelQR = labelQR;
    }

    /**
     * @return the logo
     */
    public String getLogo() {
        return logo;
    }

    /**
     * @param logo the logo to set
     */
    public void setLogo(String logo) {
        this.logo = logo;
    }

    

    /**
     * @return the medioPago
     */
    public String getMedioPago() {
        return medioPago;
    }

    /**
     * @param medioPago the medioPago to set
     */
    public void setMedioPago(String medioPago) {
        this.medioPago = medioPago;
    }

    /**
     * @return the codigoUnico
     */
    public String getCodigoUnico() {
        return codigoUnico;
    }

    /**
     * @param codigoUnico the codigoUnico to set
     */
    public void setCodigoUnico(String codigoUnico) {
        this.codigoUnico = codigoUnico;
    }

    /**
     * @return the nombreComercio
     */
    public String getNombreComercio() {
        return nombreComercio;
    }

    /**
     * @param nombreComercio the nombreComercio to set
     */
    public void setNombreComercio(String nombreComercio) {
        this.nombreComercio = nombreComercio;
    }

    JLabel lblProgreso;
    private String rutaDestino;
    private String nameOut;
    private String medioPago;
    private String codigoUnico;
    private String nombreComercio;
    private String monto;
    private String terminal;
    private String logo;
    private boolean isRunning;
    private JLabel labelQR;

    HiloInterno(String t,String m, String cu, String nc, JLabel lbl, String rd, String n,String v) {
        lblProgreso = lbl;
        rutaDestino = rd;
        nameOut = n;
        medioPago = m;
        codigoUnico = cu;
        nombreComercio = nc;
        isRunning = true;
        terminal =t;
        monto =v;
    }

    public void kill() {

        isRunning = false;
    }

    public void run() {
        
        String dataRes = "";
        String content = "000201"
                + "550201"
                + "010211"
                + "5802CO"
                + "5915COLCHONES SERTA"
                + "49250103RBM0014CO.COM.RBM.RED"
                + "9030"
                + "80260102IM0016CO.COM.RBM.CANAL"
                + "91240102820014CO.COM.RBM.SEC"
                + "81250102030015CO.COM.RBM.CIVA"
                + "60131100100 BOGOT"
                + "8226010416.00014CO.COM.RBM.IVA"
                + "50290108120991560013CO.COM.RBM.CU"
                + "8324010100015CO.COM.RBM.BASE"
                + "62180708QIJ02001080200"
                + "84250102030015CO.COM.RBM.CINC"
                + "52045712"
                + "8523010100014CO.COM.RBM.INC"
                + "5303170"
                + "64250002ES0115COLCHONES SERTA"
                + "630464EB";
        dataRes += UTILSQR.addTLVData(UTILSQR.TAG_QR_PAYLOAD, "01");
        dataRes += UTILSQR.addTLVData("55", "01");
        dataRes += UTILSQR.addTLVData(UTILSQR.TAG_QR_TIPO, "11");
        dataRes += UTILSQR.addTLVData(UTILSQR.TAG_QR_PAIS_ALFA, "CO");
        dataRes += UTILSQR.addTLVData(UTILSQR.TAG_QR_NOMBRE_COMERCIO, this.nombreComercio);
        
        dataRes += UTILSQR.addTLVData(UTILSQR.TAG_QR_MEDIO_PAGO, this.medioPago);
        String amount = this.monto.replaceAll("[^0-9]","");
        dataRes += UTILSQR.addTLVData(UTILSQR.TAG_QR_MONTO, amount);
        
        
        dataRes += UTILSQR.addTLVData(UTILSQR.TAG_QR_ADQ_ID, "0103RBM0014CO.COM.RBM.RED");
        dataRes += UTILSQR.addTLVData(UTILSQR.TAG_QR_TRANSACTION_ID, "01060000000016CO.COM.RBM.TRXID");
        dataRes += UTILSQR.addTLVData(UTILSQR.TAG_QR_CANAL, "0102IM0016CO.COM.RBM.CANAL");
        dataRes += UTILSQR.addTLVData(UTILSQR.TAG_QR_SECURE_CODE, "0102820014CO.COM.RBM.SEC");
        dataRes += UTILSQR.addTLVData(UTILSQR.TAG_QR_CONDICION_IVA, "0102030015CO.COM.RBM.CIVA");
        dataRes += UTILSQR.addTLVData(UTILSQR.TAG_QR_CIUDAD, "1100100 BOGOT");
        dataRes += UTILSQR.addTLVData(UTILSQR.TAG_QR_IVA, "010416.00014CO.COM.RBM.IVA");
        
        String tag50 = UTILSQR.addTLVData(UTILSQR.TAG_QR_50_CU, this.codigoUnico);
        tag50 += UTILSQR.addTLVData(UTILSQR.TAG_QR_50_RED, "CO.COM.RBM.CU");

        dataRes += UTILSQR.addTLVData(UTILSQR.TAG_QR_CU, tag50);
        
        
        dataRes += UTILSQR.addTLVData(UTILSQR.TAG_QR_BASE_IVA, "010100015CO.COM.RBM.BASE");

        String tag62 = UTILSQR.addTLVData(UTILSQR.TAG_QR_62_TERMINAL, this.terminal);
        tag62 += UTILSQR.addTLVData(UTILSQR.TAG_QR_62_TIPO_OPERACION, "00");

        dataRes += UTILSQR.addTLVData(UTILSQR.TAG_QR_CAMPO_62, tag62);

        dataRes += UTILSQR.addTLVData(UTILSQR.TAG_QR_CONDICION_INC, "0102030015CO.COM.RBM.CINC");
        dataRes += UTILSQR.addTLVData(UTILSQR.TAG_QR_MCC, "5712");
        dataRes += UTILSQR.addTLVData(UTILSQR.TAG_QR_INC, "010100014CO.COM.RBM.INC");

        dataRes += UTILSQR.addTLVData(UTILSQR.TAG_QR_PAIS_NUMERICO, "170");

        String tag64 = UTILSQR.addTLVData(UTILSQR.TAG_QR_64_LENGUAJE, "ES");
        tag64 += UTILSQR.addTLVData(UTILSQR.TAG_QR_64_COMERCIO, this.nombreComercio);

        dataRes += UTILSQR.addTLVData(UTILSQR.TAG_QR_CAMPO_64, tag64);

        long crc = UTILSQR.crc16(dataRes+"6304");
        String CRCString = Integer.toHexString((int) crc);
        dataRes += UTILSQR.addTLVData(UTILSQR.TAG_QR_CAMPO_CRC, CRCString.toUpperCase());

        File imgLogo = GeneradorQR.generate(dataRes, logo, "./outLogo_QR");
        if(imgLogo!=null)
        {
        imgLogo = GeneradorQR.addStringsImage(
                imgLogo,
                0,
                imgLogo.getAbsolutePath(),
                nombreComercio,
                "Cod. Comercio: " + codigoUnico.replaceFirst("00", ""),
                " ");
        imgLogo = GeneradorQR.addStringsImage(
                imgLogo,
                1,
                imgLogo.getAbsolutePath(),
                "Medio de Pago: *****" + medioPago.substring(medioPago.length()-4),
                " ");
        }

        File f = new File("./outLogo_QR.png");
        
        try {
            
        Image bi = ImageIO.read(f);
        int size = labelQR.getParent().getParent().getHeight();
        size =(size /2);
        labelQR.setIcon(new ImageIcon(bi.getScaledInstance(size, size+(size/3), size)));
        labelQR.setHorizontalAlignment(CENTER);
        }
                catch (IOException ex) {
            Logger.getLogger(QRFrame.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public BufferedImage rescale(BufferedImage originalImage) {
        int baseSize = 128;
        BufferedImage resizedImage = new BufferedImage(baseSize, baseSize, BufferedImage.TYPE_INT_RGB);
        Graphics2D g = resizedImage.createGraphics();
        g.drawImage(originalImage, 0, 0, baseSize, baseSize, null);
        g.dispose();
        return resizedImage;
    }
}
