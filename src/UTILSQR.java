
import com.google.zxing.BarcodeFormat;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.ChecksumException;
import com.google.zxing.DecodeHintType;
import com.google.zxing.FormatException;
import com.google.zxing.LuminanceSource;
import com.google.zxing.NotFoundException;
import com.google.zxing.Reader;
import com.google.zxing.Result;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.qrcode.QRCodeReader;
import java.util.List;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.Map;
import java.util.zip.CRC32;
import java.util.zip.Checksum;
import javax.imageio.ImageIO;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author jmtoro
 */
public class UTILSQR {
    public static final String TAG_QR_PAYLOAD ="00";
    public static final String TAG_QR_TIPO ="01";
    
    
    public static final String TAG_QR_MEDIO_PAGO ="48";
    
    public static final String TAG_QR_ADQ_ID ="49";
    public static final String TAG_QR_CU ="50";
    public static final String TAG_QR_AGREGADOR_ID ="51";
    public static final String TAG_QR_MCC ="52";
    
    public static final String TAG_QR_PAIS_NUMERICO ="53";
    
    public static final String TAG_QR_MONTO ="54";
    
    public static final String TAG_QR_INDICADOR_PROPINA ="55";
    
    public static final String TAG_QR_PAIS_ALFA ="58";
    public static final String TAG_QR_NOMBRE_COMERCIO ="59";
    public static final String TAG_QR_CIUDAD ="60";
    public static final String TAG_QR_CODIGO_POSTAL ="61";
    
    public static final String TAG_QR_CAMPO_62 ="62";
    
    public static final String TAG_QR_CAMPO_CRC ="63";
    
    public static final String TAG_QR_CAMPO_64 ="64";
    
    
    public static final String TAG_QR_CRC ="63";
    
    public static final String TAG_QR_CANAL ="80";
    public static final String TAG_QR_CONDICION_IVA ="81";
    public static final String TAG_QR_IVA ="82";
    
    public static final String TAG_QR_BASE_IVA ="83";
    
    public static final String TAG_QR_CONDICION_INC ="84";
    public static final String TAG_QR_INC ="85";
    
    public static final String TAG_QR_TRANSACTION_ID ="90";
    public static final String TAG_QR_SECURE_CODE ="91";
    
    
    public static final String TAG_QR_62_TERMINAL ="07";
    public static final String TAG_QR_62_TIPO_OPERACION ="08";
    
    public static final String TAG_QR_50_CU ="01";
    public static final String TAG_QR_50_RED ="00";
    
    public static final String TAG_QR_64_LENGUAJE ="00";
    public static final String TAG_QR_64_COMERCIO ="01";
    
    public static final String TAG_QR_80_RED ="00";
    public static final String TAG_QR_80_CANAL ="01";
    
    public static final String TAG_QR_81_RED ="00";
    public static final String TAG_QR_81_VALOR ="01";
    
    public static final String TAG_QR_82_RED ="00";
    public static final String TAG_QR_82_VALOR ="01";
    
    public static final String TAG_QR_83_RED ="00";
    public static final String TAG_QR_83_VALOR ="01";
    
    public static final String TAG_QR_84_RED ="00";
    public static final String TAG_QR_84_VALOR ="01";
    
    public static final String TAG_QR_85_RED ="00";
    public static final String TAG_QR_85_VALOR ="01";
    
    public static final String TAG_QR_90_RED ="00";
    public static final String TAG_QR_90_VALOR ="01";
    
    public static final String TAG_QR_91_RED ="00";
    public static final String TAG_QR_91_VALOR ="01";
    
    public static final String TAG_QR_ADICIONAL_RED ="00";
    public static final String TAG_QR_ADICIONAL_VALOR ="01";
    
    
    
    private static String decodeQRCode(File qrCodeimage) throws IOException {
        
        String res = null;

        try {
            Map<DecodeHintType, Object> tmpHintsMap = new EnumMap<>(DecodeHintType.class);
            tmpHintsMap.put(DecodeHintType.TRY_HARDER, Boolean.TRUE);
            tmpHintsMap.put(DecodeHintType.POSSIBLE_FORMATS, EnumSet.allOf(BarcodeFormat.class));
            tmpHintsMap.put(DecodeHintType.PURE_BARCODE, Boolean.FALSE);
            

            Reader lectorQR = new QRCodeReader();

            File ubicacionImagen = qrCodeimage;
            BufferedImage imagen;
            imagen = ImageIO.read(ubicacionImagen);
            LuminanceSource fuente = new BufferedImageLuminanceSource(imagen);
            BinaryBitmap mapaBits = new BinaryBitmap(new HybridBinarizer(fuente));

            Result result = lectorQR.decode(mapaBits, tmpHintsMap);
            res = result.getText();
        } catch (ChecksumException | FormatException | NotFoundException | IOException e) {
            System.out.println("There is no QR code in the image " + e.getMessage());
        }
        return res;
    }

    public static File getDataQR(String input, String path) {
        String in = input.substring(2);
        String sizeImg = input.substring(0,2);
        int factor =2;
        
        byte[] finalArrayT = in.getBytes();
        byte[] decodedBytesT = Base64.getDecoder().decode(finalArrayT);
        PlainQR.generateQR(decodedBytesT, Integer.parseInt(sizeImg),factor,path);
        return new File(path);
    }
    
    
    public static String getDecodedDataQR(String input){
        String outData=null;
        try {
            File file = new File(input);
            String decodedText = decodeQRCode(file);
            if(decodedText == null) {
                System.out.println("No QR Code found in the image");
            } else {
                System.out.println("Decoded text = " + decodedText);
                outData = decodedText;
            }
        } catch (IOException e) {
            System.out.println("Could not decode QR Code, IOException :: " + e.getMessage());
        }
        return outData;
    }
    
        public static List<TLV_Struct> getTLVData(String input){
        
        // Obj is the type of object to be stored in List
        List<TLV_Struct> list = new ArrayList<>();
        
        int index =0;
        try {
            do{
            TLV_Struct outData= new TLV_Struct();
            outData.setTag(input.substring(index,index+2));
            index+=2;
            
            outData.setLen(input.substring(index,index+2));
            index+=2;
            
            int lenInteger = Integer.parseInt(outData.getLen());
            
            outData.setValue(input.substring(index,index+lenInteger));
            index+=lenInteger;
            list.add(outData);
            }
            while(index<input.length());
            
        } catch (NumberFormatException e) {
            System.out.println("Could not decode QR Code, IOException :: " + e.getMessage());
        }
        return list;
    }
        
        
    public static String searchTLVData(String tag, List<TLV_Struct> input){
        
        String dataOut=null;
        
        int index =0;
        
        

            while (index < input.size()) {
                TLV_Struct registro = input.get(index);
                if( true == registro.getTag().equals(tag)){
                    dataOut = registro.getValue();
                    break;
                }
                
                index++;
            }
        return dataOut;
    }
    
    public static String addTLVData(String tag,String value){
        String res="";
        String dataClear = value.replaceAll("[^A-Za-z0-9 .]", "").toUpperCase();
        int len = dataClear.length();
        
        res +=tag;
        res += String.format("%02d" , len);
        res += dataClear;
        return res;
    }
    
     public static long crc32(String input) {
        byte[] bytes = input.getBytes();
        Checksum checksum = new CRC32(); // java.util.zip.CRC32
        checksum.update(bytes, 0, bytes.length);

        return checksum.getValue();
    }
     
    private static final int POLYNOMIAL   = 0x1021;
    private static final int PRESET_VALUE = 0xFFFF;

    public static int crc16(String input) {
        byte[] data = input.getBytes();
        int current_crc_value = PRESET_VALUE;
        for (int i = 0; i < data.length; i++) {
            current_crc_value ^= data[i] & 0xFF;
            for (int j = 0; j < 8; j++) {
                if ((current_crc_value & 1) != 0) {
                    current_crc_value = (current_crc_value >>> 1) ^ POLYNOMIAL;
                } else {
                    current_crc_value = current_crc_value >>> 1;
                }
            }
        }
        current_crc_value = ~current_crc_value;

        return current_crc_value & 0xFFFF;
    }
}
