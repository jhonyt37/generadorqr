import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import javax.swing.JLabel;
 
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public final class FicheroSalida {
    
String nombreArchivo = "";
String rutaArchivo = "";
String hoja = "Hoja1";
JLabel lblSalida;
RegistroExcel[] matriz = null;

    FicheroSalida(int size) {
        setMatrizSize(size);
    }
    
    FicheroSalida(int size,String salida, String ruta) {
        setMatrizSize(size);
        setFileName(salida,ruta);
    }
    
    FicheroSalida(int size,String salida, String ruta, JLabel lbl) {
        setMatrizSize(size);
        setFileName(salida,ruta);
        lblSalida = lbl;
    }
    
    void setMatrizSize(int len) {
        this.matriz = new RegistroExcel[len];
        for (int i = 0; i < len; i++) {
            this.matriz[i] = new RegistroExcel();
        }
    }
    
    void setMatrizData(int index,String CU, String nombreComercio, String dataQR){
        if(index< this.matriz.length){
        this.matriz[index].setCU(CU);
        this.matriz[index].setNombreComercio(nombreComercio);
        this.matriz[index].setDataQR(dataQR);
        }
        else{
           QRFrame.serverLogger.escribeLogInfo("error indice especificado "+index); 
        }
    }
    
        void setMatrizData(int index,
            String CU) {
        if (index < this.matriz.length) {
            this.matriz[index].setCU(CU);

        } else{
           QRFrame.serverLogger.escribeLogInfo("error indice especificado "+index); 
        }
    }

    void setMatrizData(int index,
                       String CU, 
                       String nombreComercio){
        if(index< this.matriz.length){
        this.matriz[index].setCU(CU);
        this.matriz[index].setNombreComercio(nombreComercio);
        
        }
        else{
           QRFrame.serverLogger.escribeLogInfo("error indice especificado "+index); 
        }
    }
    
        void setMatrizData(int index,
            String CU,
            String nombreComercio,
            String dataQR,
            String terminal) {
        if (index < this.matriz.length) {
            this.matriz[index].setCU(CU);
            this.matriz[index].setNombreComercio(nombreComercio);
            this.matriz[index].setDataQR(dataQR);

            this.matriz[index].setTerminalComercio(terminal);

        }
        else{
           QRFrame.serverLogger.escribeLogInfo("error indice especificado "+index); 
        }
    }
        
        void setMatrizData(int index,
                       String CU, 
                       String nombreComercio, 
                       String dataQR,
                       String terminal,
                       String ciudad){
        if(index< this.matriz.length){
        this.matriz[index].setCU(CU);
        this.matriz[index].setNombreComercio(nombreComercio);
        this.matriz[index].setDataQR(dataQR);
        
        this.matriz[index].setTerminalComercio(terminal);
        this.matriz[index].setCiudad(ciudad);
        
        }
        else{
           QRFrame.serverLogger.escribeLogInfo("error indice especificado "+index); 
        }
    }
        
        void setMatrizData(int index,
                       String CU, 
                       String nombreComercio, 
                       String dataQR,
                       String terminal,
                       String ciudad,
                       String iva){
        if(index< this.matriz.length){
        this.matriz[index].setCU(CU);
        this.matriz[index].setNombreComercio(nombreComercio);
        this.matriz[index].setDataQR(dataQR);
        
        this.matriz[index].setTerminalComercio(terminal);
        this.matriz[index].setCiudad(ciudad);
        this.matriz[index].setImpuestoIVA(iva);
        
        }
        else{
           QRFrame.serverLogger.escribeLogInfo("error indice especificado "+index); 
        }
    }
    
        void setMatrizData(int index,
                       String CU, 
                       String nombreComercio, 
                       String dataQR,
                       String terminal,
                       String ciudad,
                       String iva,
                       String inputData){
        if(index< this.matriz.length){
        this.matriz[index].setCU(CU);
        this.matriz[index].setNombreComercio(nombreComercio);
        this.matriz[index].setDataQR(dataQR);
        
        this.matriz[index].setTerminalComercio(terminal);
        this.matriz[index].setCiudad(ciudad);
        
        this.matriz[index].setInputQR(inputData); 
        this.matriz[index].setImpuestoIVA(iva);
        
        }
        else{
           QRFrame.serverLogger.escribeLogInfo("error indice especificado "+index); 
        }
    }
        
    void setMatrizData(int index,
                       String CU, 
                       String nombreComercio, 
                       String dataQR,
                       String terminal,
                       String ciudad,
                       String iva,
                       String inputData,
                       File img){
        if(index< this.matriz.length){
        this.matriz[index].setCU(CU);
        this.matriz[index].setNombreComercio(nombreComercio);
        this.matriz[index].setDataQR(dataQR);
        
        this.matriz[index].setTerminalComercio(terminal);
        this.matriz[index].setCiudad(ciudad);
        
        this.matriz[index].setInputQR(inputData); 
        this.matriz[index].setImpuestoIVA(iva);
        this.matriz[index].setImagenLogo(img);
        
        }
        else{
           QRFrame.serverLogger.escribeLogInfo("error indice especificado "+index); 
        }
    }
    
        void setMatrizData(int index,
                       String CU, 
                       String nombreComercio, 
                       String dataQR,
                       String terminal,
                       String ciudad,
                       String iva,
                       String inputData,
                       File imgLogo,
                       File imgOriginal){
        if(index< this.matriz.length){
        this.matriz[index].setCU(CU);
        this.matriz[index].setNombreComercio(nombreComercio);
        this.matriz[index].setDataQR(dataQR);
        
        this.matriz[index].setTerminalComercio(terminal);
        this.matriz[index].setCiudad(ciudad);
        
        this.matriz[index].setInputQR(inputData); 
        this.matriz[index].setImpuestoIVA(iva);
        this.matriz[index].setImagenLogo(imgLogo);
        this.matriz[index].setImagenOriginal(imgOriginal);
        
        }
        else{
           QRFrame.serverLogger.escribeLogInfo("error indice especificado "+index); 
        }
    }
    void setFileName(String salida, String ruta) {
        this.nombreArchivo = salida;
        this.rutaArchivo = ruta + nombreArchivo;
        this.hoja = "Hoja1";
    }
    
    private final int WIDTH_LOGO =340*33;
    private final int WIDTH_QR_PLAIN =340*40;
    private final int HEIGHT_LOGO =340*18;
    public void crearExcelsalida() {

        if (this.nombreArchivo != null && this.rutaArchivo != null) {

            XSSFWorkbook libro = new XSSFWorkbook();
            XSSFSheet hoja1 = libro.createSheet(hoja);

            
            CellStyle style = libro.createCellStyle();
            CellStyle headerStyle = libro.createCellStyle();
            
            style.setVerticalAlignment(VerticalAlignment.CENTER);
            style.setWrapText(true);

            headerStyle.setVerticalAlignment(VerticalAlignment.CENTER);
            headerStyle.setAlignment(HorizontalAlignment.CENTER);
            headerStyle.setWrapText(true);
            headerStyle.setFillBackgroundColor(IndexedColors.BLUE.getIndex());
            headerStyle.setFillForegroundColor(IndexedColors.YELLOW.getIndex());
            headerStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
            headerStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());
            headerStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
            headerStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());
            headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

            lblSalida.setText("Almacenando: 0%");
            QRFrame.serverLogger.escribeLogInfo("Almacenando: 0%");
            float steps = 100 / (float) matriz.length;
            //generar los datos para el documento
            XSSFRow row = hoja1.createRow(0);//se crea las filas
            short height = 500;
            row.setHeight(height);

            row.setHeight(height);
            row.setRowStyle(headerStyle);
            XSSFCell cell = row.createCell(0);//se crea las celdas para la cabecera, junto con la posición
            cell.setCellStyle(headerStyle); // se añade el style crea anteriormente 
            cell.setCellValue("Nombre Comercio");//se añade el contenido					

            cell = row.createCell(1);//se crea las celdas para la cabecera, junto con la posición
            cell.setCellStyle(headerStyle); // se añade el style crea anteriormente 
            cell.setCellValue("Codigo Unico");//se añade el contenido					

            cell = row.createCell(2);//se crea las celdas para la cabecera, junto con la posición
            cell.setCellStyle(headerStyle); // se añade el style crea anteriormente 
            cell.setCellValue("Terminal");//se añade el contenido					

            cell = row.createCell(3);//se crea las celdas para la cabecera, junto con la posición
            cell.setCellStyle(headerStyle); // se añade el style crea anteriormente 
            cell.setCellValue("Ciudad");//se añade el contenido					

            cell = row.createCell(4);//se crea las celdas para la cabecera, junto con la posición
            cell.setCellStyle(headerStyle); // se añade el style crea anteriormente 
            cell.setCellValue("IVA");//se añade el contenido					

            cell = row.createCell(5);//se crea las celdas para la cabecera, junto con la posición
            cell.setCellStyle(headerStyle); // se añade el style crea anteriormente 
            cell.setCellValue("Imagen");//se añade el contenido					

            cell = row.createCell(6);//se crea las celdas para la cabecera, junto con la posición
            cell.setCellStyle(headerStyle); // se añade el style crea anteriormente 
            cell.setCellValue("Imagen Original QR");//se añade el contenido					

            cell = row.createCell(7);//se crea las celdas para la cabecera, junto con la posición
            cell.setCellStyle(headerStyle); // se añade el style crea anteriormente 
            cell.setCellValue("Salida QR");//se añade el contenido					

            cell = row.createCell(8);//se crea las celdas para la cabecera, junto con la posición
            cell.setCellStyle(headerStyle); // se añade el style crea anteriormente 
            cell.setCellValue("Entrada");//se añade el contenido					

            for (int colum = 0; colum < 9; colum++) {
                hoja1.autoSizeColumn(colum);
                if (colum == 7 || colum == 8) {
                    hoja1.setColumnWidth(colum, WIDTH_QR_PLAIN);
                }
                if (colum == 6) {
                    hoja1.setColumnWidth(colum, WIDTH_QR_PLAIN);
                }
                if (colum == 5 ) {
                    hoja1.setColumnWidth(colum, WIDTH_LOGO);
                }
            }
            for (int i = 0; i < matriz.length; i++) {
                try {

                    lblSalida.setText("Almacenando: " + (int) (i * steps) + "%");
                    QRFrame.serverLogger.escribeLogInfo("Almacenando: " + (int) (i * steps) + "%");
                    row = hoja1.createRow(i+1);//se crea las filas
                    height = HEIGHT_LOGO;
                    row.setHeight(height);

                    cell = row.createCell(0);//se crea las celdas para la contenido, junto con la posición
                    cell.setCellStyle(style); // se añade el style crea anteriormente 
                    cell.setCellValue(matriz[i].getNombreComercio()); //se añade el contenido

                    cell = row.createCell(1);//se crea las celdas para la contenido, junto con la posición
                    cell.setCellStyle(style); // se añade el style crea anteriormente 
                    cell.setCellValue(matriz[i].getCU()); //se añade el contenido

                    cell = row.createCell(2);//se crea las celdas para la contenido, junto con la posición
                    cell.setCellStyle(style); // se añade el style crea anteriormente 
                    cell.setCellValue(matriz[i].getTerminalComercio()); //se añade el contenido

                    cell = row.createCell(3);//se crea las celdas para la contenido, junto con la posición
                    cell.setCellStyle(style); // se añade el style crea anteriormente 
                    cell.setCellValue(matriz[i].getCiudad()); //se añade el contenido

                    cell = row.createCell(4);//se crea las celdas para la contenido, junto con la posición
                    cell.setCellStyle(style); // se añade el style crea anteriormente 
                    cell.setCellValue(matriz[i].getImpuestoIVA()); //se añade el contenido

                    cell = row.createCell(5);//se crea las celdas para la contenido, junto con la posición

                    InputStream inputStream = new FileInputStream(matriz[i].getImagenLogo());
                    byte[] imageBytes = IOUtils.toByteArray(inputStream);
                    int pictureureIdx = libro.addPicture(imageBytes, libro.PICTURE_TYPE_PNG);
                    inputStream.close();
                    CreationHelper helper = libro.getCreationHelper();
                    Drawing drawing = hoja1.createDrawingPatriarch();
                    ClientAnchor anchor = helper.createClientAnchor();

                    anchor.setCol1(5);
                    anchor.setRow1(row.getRowNum());
                    anchor.setCol2(6);
                    anchor.setRow2(row.getRowNum() + 1);

                    drawing.createPicture(anchor, pictureureIdx);

                    cell = row.createCell(6);//se crea las celdas para la contenido, junto con la posición

                    inputStream = new FileInputStream(matriz[i].getImagenOriginal());
                    imageBytes = IOUtils.toByteArray(inputStream);
                    pictureureIdx = libro.addPicture(imageBytes, libro.PICTURE_TYPE_PNG);
                    inputStream.close();
                    helper = libro.getCreationHelper();
                    drawing = hoja1.createDrawingPatriarch();
                    anchor = helper.createClientAnchor();

                    anchor.setCol1(6);
                    anchor.setRow1(row.getRowNum());
                    anchor.setCol2(7);
                    anchor.setRow2(row.getRowNum() + 1);

                    drawing.createPicture(anchor, pictureureIdx);

                    cell = row.createCell(7);//se crea las celdas para la contenido, junto con la posición
                    cell.setCellStyle(style); // se añade el style crea anteriormente 
                    cell.setCellValue(matriz[i].getDataQR()); //se añade el contenido

                    cell = row.createCell(8);//se crea las celdas para la contenido, junto con la posición
                    cell.setCellStyle(style); // se añade el style crea anteriormente 
                    cell.setCellValue(matriz[i].getInputQR()); //se añade el contenido
                } catch (Exception ex) {
                    ex.getMessage();
                }
            }

            lblSalida.setText("Almacenando: 99%");
            QRFrame.serverLogger.escribeLogInfo("Almacenando: 99%");
            File file;
            file = new File(rutaArchivo);
            try (FileOutputStream fileOuS = new FileOutputStream(file)) {
                if (file.exists()) {// si el archivo existe se elimina
                    file.delete();
                    QRFrame.serverLogger.escribeLogInfo("Archivo eliminado");
                }
                libro.write(fileOuS);
                fileOuS.flush();
                fileOuS.close();
                QRFrame.serverLogger.escribeLogInfo("Archivo Creado");

            } catch (FileNotFoundException e) {
                QRFrame.serverLogger.escribeLogInfo("error archivo " + e.getMessage());
            } catch (IOException e) {
                QRFrame.serverLogger.escribeLogInfo("error archivo out " + e.getMessage());
            }
        } else {
            QRFrame.serverLogger.escribeLogInfo("error parametros no configurados");
        }
        lblSalida.setText("Almacenando: 100%");
        QRFrame.serverLogger.escribeLogInfo("Almacenando: 100%");

    }


}
