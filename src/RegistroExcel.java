
import java.io.File;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author jmtoro
 */
public class RegistroExcel {
   private static final int size =3;
   private String CU="";
   private String nombreComercio="";
   private String terminalComercio="";
   private String ciudad="";
   private String impuestoIVA="";
   private String inputQR="";
   private File imagenLogo;
   private File imagenOriginal;
   private String dataQR="";

    /**
     * @param CU the CU to set
     */
    public void setCU(String CU) {
        this.CU = CU;
    }

    /**
     * @param nombreComercio the nombreComercio to set
     */
    public void setNombreComercio(String nombreComercio) {
        this.nombreComercio = nombreComercio;
    }

    /**
     * @param dataQR the dataQR to set
     */
    public void setDataQR(String dataQR) {
        this.dataQR = dataQR;
    }

    /**
     * @return the CU
     */
    public String getCU() {
        return CU;
    }

    /**
     * @return the nombreComercio
     */
    public String getNombreComercio() {
        return nombreComercio;
    }

    /**
     * @return the dataQR
     */
    public String getDataQR() {
        return dataQR;
    }

    /**
     * @return the size
     */
    public static int getSize() {
        return size;
    }

    /**
     * @return the terminalComercio
     */
    public String getTerminalComercio() {
        return terminalComercio;
    }

    /**
     * @param terminalComercio the terminalComercio to set
     */
    public void setTerminalComercio(String terminalComercio) {
        this.terminalComercio = terminalComercio;
    }

    /**
     * @return the ciudad
     */
    public String getCiudad() {
        return ciudad;
    }

    /**
     * @param ciudad the ciudad to set
     */
    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    /**
     * @return the impuestoIVA
     */
    public String getImpuestoIVA() {
        return impuestoIVA;
    }

    /**
     * @param impuestoIVA the impuestoIVA to set
     */
    public void setImpuestoIVA(String impuestoIVA) {
        this.impuestoIVA = impuestoIVA;
    }

    /**
     * @return the inputQR
     */
    public String getInputQR() {
        return inputQR;
    }

    /**
     * @param inputQR the inputQR to set
     */
    public void setInputQR(String inputQR) {
        this.inputQR = inputQR;
    }

    /**
     * @return the imagenLogo
     */
    public File getImagenLogo() {
        return imagenLogo;
    }

    /**
     * @param imagenLogo the imagenLogo to set
     */
    public void setImagenLogo(File imagenLogo) {
        this.imagenLogo = imagenLogo;
    }

    /**
     * @return the imagenOriginal
     */
    public File getImagenOriginal() {
        return imagenOriginal;
    }

    /**
     * @param imagenOriginal the imagenOriginal to set
     */
    public void setImagenOriginal(File imagenOriginal) {
        this.imagenOriginal = imagenOriginal;
    }
   
}
