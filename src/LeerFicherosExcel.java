import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
 
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
 
public class LeerFicherosExcel {
 
	public static void LeerExcel( File entrada) {
            entrada.getName();
		String nombreArchivo = entrada.getName();
		String rutaArchivo = "" + entrada.getAbsolutePath();
		String hoja = "Hoja1";
 
		try (FileInputStream file = new FileInputStream(new File(rutaArchivo))) {
			// leer archivo excel
			XSSFWorkbook worbook = new XSSFWorkbook(file);
			//obtener la hoja que se va leer
			XSSFSheet sheet = worbook.getSheetAt(0);
			//obtener todas las filas de la hoja excel
			Iterator<Row> rowIterator = sheet.iterator();
 
			Row row;
			// se recorre cada fila hasta el final
			while (rowIterator.hasNext()) {
				row = rowIterator.next();
				//se obtiene las celdas por fila
				Iterator<Cell> cellIterator = row.cellIterator();
				Cell cell;
				//se recorre cada celda
				while (cellIterator.hasNext()) {
					// se obtiene la celda en específico y se la imprime
					cell = cellIterator.next();
					System.out.print(cell.getStringCellValue()+" | ");
				}
                                QRFrame.serverLogger.escribeLogInfo("");
			}
		} catch (Exception e) {
                    Logger.getLogger(QRFrame.class.getName()).log(Level.SEVERE, null, e.getMessage());
                    QRFrame.serverLogger.escribeLogInfo("error en entrada22 "+e.getMessage());
                    JOptionPane.showConfirmDialog(null, e.getMessage());
			
		}
	}
        
        
        private static String replaceSpecialChars(String in){
        String res="";
        String dataError ="";
        System.out.println( res = in.replaceAll("[^\\dA-Za-z]", ""));
        return res;
    }
    public static void LeerExcel( File entrada, JLabel lblSalida,String rd,String out) {
		
        String rutaArchivo = "" + entrada.getAbsolutePath();
        String resto = "" + entrada.getName();
        String rutaDestino = rd+"\\";
        String hoja = "Hoja1";
        
        QRFrame.serverLogger.escribeLogInfo("paso 1 " + rutaArchivo);
        QRFrame.serverLogger.escribeLogInfo("paso 2 " + resto);
        QRFrame.serverLogger.escribeLogInfo("paso 3 " + rutaDestino);

        FicheroSalida salida = null;
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";
        lblSalida.setText("Progreso : 0%");
        try {

            float fileSize =0;
            try (FileInputStream file = new FileInputStream(new File(rutaArchivo))) {
                // leer archivo excel
			XSSFWorkbook worbook = new XSSFWorkbook(file);
			//obtener la hoja que se va leer
			XSSFSheet sheet = worbook.getSheetAt(0);
			//obtener todas las filas de la hoja excel
			Iterator<Row> rowIterator = sheet.iterator();
                        
                        fileSize = sheet.getLastRowNum();
                        lblSalida.setText("Progreso : 1%");
                        QRFrame.serverLogger.escribeLogInfo("Progreso : 1% tamaño de archivo"+fileSize+ "registros");
 
                        float steps = 100/fileSize;
                        QRFrame.serverLogger.escribeLogInfo("filesize = "+fileSize+ " "+steps);
            
			Row row;
			// se recorre cada fila hasta el final
			while (rowIterator.hasNext()) {
				row = rowIterator.next();
				//se obtiene las celdas por fila
				Iterator<Cell> cellIterator = row.cellIterator();
				Cell cell;
				//se recorre cada celda
				while (cellIterator.hasNext()) {
					// se obtiene la celda en específico y se la imprime
					cell = cellIterator.next();
					System.out.print(cell.getStringCellValue()+" | ");
				}
                                QRFrame.serverLogger.escribeLogInfo("");
			}
            }
            /*
            
            br = new BufferedReader(new FileReader(rutaArchivo));
            int i = 0, j=0;
            while ((line = br.readLine()) != null) {
                // use comma as separator
                line = line.replace("\"", "");
                String[] lineIn = line.split(cvsSplitBy);
                QRFrame.serverLogger.escribeLogInfo("len " + lineIn.length);
                QRFrame.serverLogger.escribeLogInfo("lenData " + lineIn[0]);
                lineIn[0] = replaceSpecialChars(lineIn[0]);
                int cu=-1;
                try{
                    Integer.parseInt(lineIn[0]);
                    cu =0;
                }
                catch(Exception ex){
                    QRFrame.serverLogger.escribeLogInfo("registro no procesado "+lineIn[0]);
                }
                        
                if(lineIn[0].equals("Codigo Unico") || lineIn[0].endsWith("ico") || j==0){
                    QRFrame.serverLogger.escribeLogInfo("Archivo de entrada OK");
                    salida = new FicheroSalida((int)fileSize,out,rutaDestino,lblSalida);
                    j++;
                    lblSalida.setText("Progreso : 2%");
                    QRFrame.serverLogger.escribeLogInfo("Progreso : 2%");
                }
                else if(lineIn.length==4 && cu==0){
                    String dataRes = utilsQR.getDataQR(lineIn[3]);
                salida.setMatrizData(i, lineIn[0]+" - " +lineIn[1], lineIn[2],dataRes);
                i++;
                int jj=0;
                while ( jj < lineIn.length) {
                    QRFrame.serverLogger.escribeLogInfo(lineIn[jj] + " ");
                    jj++;
                }
                QRFrame.serverLogger.escribeLogInfo("");
                }
                else{
                    QRFrame.serverLogger.escribeLogInfo("data de entrada invalida entrada "+lineIn.length);
                }
                lblSalida.setText("Progreso : "+(int)(i*steps)+"%");
                QRFrame.serverLogger.escribeLogInfo("Progreso : "+(int)(i*steps)+"%");
            }
            lblSalida.setText("Progreso : 99% total registros procesados: "+i);
            QRFrame.serverLogger.escribeLogInfo("Progreso : 99% total registros procesados: "+i);
            
            salida.crearExcelsalida();
            lblSalida.setText("Progreso : 100%");
            QRFrame.serverLogger.escribeLogInfo("Progreso : 100%");
            JOptionPane.showMessageDialog(null, "Proceso finalizado con exito");*/

        } catch (Exception e) {
            Logger.getLogger(QRFrame.class.getName()).log(Level.SEVERE, null, e.getMessage());
            QRFrame.serverLogger.escribeLogInfo("error en entrada " + e.getMessage());
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }
}