import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageConfig;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.FileVisitOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Comparator;
import java.util.EnumMap;
import java.util.Map;
import java.util.Random;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author jmtoro
 */
public class GeneradorQR {
    
      public static File generate(String content,String pathLogo,String pathDest) {
          File res=null;
          final String ext = ".png";
          
        int size = 0;

        try {
            String logo_path = pathLogo;
            // Load logo image
            //C:\Users\jmtoro\Documents\NetBeansProjects\BMP\recursos\imagenes
            BufferedImage overly = getOverly(logo_path);
            if(overly!=null){
                size = overly.getWidth() + 220;
                if(overly.getWidth()>300 || overly.getHeight()>300){
                    JOptionPane.showMessageDialog(null, "Error en imagen seleccionada, maxima longitud 150 pixeles");
                    return null;
                }
            }
            else
                size = 220;
            // Create new configuration that specifies the error correction
            //Map<EncodeHintType, ErrorCorrectionLevel> hints = new HashMap<>();
            //hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);
            
            Map<EncodeHintType, Object> hints = new EnumMap<EncodeHintType, Object>(EncodeHintType.class);
            hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.Q);
            hints.put(EncodeHintType.CHARACTER_SET, "UTF-8");
            hints.put(EncodeHintType.MARGIN, 0); /* default = 4 */

            QRCodeWriter writer = new QRCodeWriter();
            BitMatrix bitMatrix = null;
            ByteArrayOutputStream os = new ByteArrayOutputStream();

            // init directory
            //cleanDirectory(DIR);
            // Create a qr code with the url as content and a size of WxH px
            bitMatrix = writer.encode(content, BarcodeFormat.QR_CODE, size, size, hints);

            // Load QR image
            BufferedImage qrImage = MatrixToImageWriter.toBufferedImage(bitMatrix, getMatrixConfig());

            // Calculate the delta height and width between QR code and logo
            int deltaHeight = qrImage.getHeight();
            int deltaWidth = qrImage.getWidth();
            if (overly != null) {
                deltaHeight = qrImage.getHeight() - overly.getHeight();
                deltaWidth = qrImage.getWidth() - overly.getWidth();
            }
            
            // Initialize combined image
            BufferedImage combined = new BufferedImage(qrImage.getHeight(), qrImage.getWidth(), BufferedImage.TYPE_INT_ARGB);
            Graphics2D g = (Graphics2D) combined.getGraphics();

            // Write QR code to new image at position 0/0
            g.drawImage(qrImage, 0, 0, null);
            g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1f));

            // Write logo into combine image at position (deltaWidth / 2) and
            // (deltaHeight / 2). Background: Left/Right and Top/Bottom must be
            // the same space for the logo to be centered
            g.drawImage(overly, (int) Math.round(deltaWidth / 2), (int) Math.round(deltaHeight / 2), null);

            // Write combined image as PNG to OutputStream
            ImageIO.write(combined, "png", os);
            // Store Image
            
            Files.copy(
                    new ByteArrayInputStream(os.toByteArray()), Paths.get(pathDest + ext),
                    StandardCopyOption.REPLACE_EXISTING);
            res = new File(pathDest + ext);
            
            
        } catch (WriterException e) {
            e.printStackTrace();
            //LOG.error("WriterException occured", e);
        } catch (IOException e) {
            e.printStackTrace();
            //LOG.error("IOException occured", e);
        }
        return res;
    }

      private static BufferedImage getOverly(String LOGO) throws IOException {
          //URL url = new URL(LOGO);
          File logo = new File(LOGO);
          if (logo.exists() && !logo.isDirectory())
              return ImageIO.read(logo);
          else
              return null;
      }

      private static BufferedImage getOverly(File fileSrc) throws IOException {
          //URL url = new URL(LOGO);
          return ImageIO.read(fileSrc);
      }
            
      private static void initDirectory(String DIR) throws IOException {
          Files.createDirectories(Paths.get(DIR));
      }

      private void cleanDirectory(String DIR) {
          try {
              Files.walk(Paths.get(DIR), FileVisitOption.FOLLOW_LINKS)
                      .sorted(Comparator.reverseOrder())
                      .map(Path::toFile)
                      .forEach(File::delete);
          } catch (IOException e) {
              // Directory does not exist, Do nothing
          }
      }

    public static final int BLACK = 0xFF000000;
    public static final int BLACK_BLUE = 0xFF031ECA;
    public static final int BLACK_RED = 0xFF910655;
    public static final int WHITE = 0xFFFFFFFF;
    public static final int BLACK_YELLOW = 0xFFA69D1D;
    public static final int BLACK_ORANGE = 0xFFEB6600;
    public static final int BLACK_GREEN = 0xFF05A220;
    
    public static int getColorDot(){
        int[] matrixColor = new int[7];
          matrixColor[0] = BLACK;
          matrixColor[1] = BLACK_BLUE;
          matrixColor[2] = BLACK_RED;
          matrixColor[3] = BLACK_YELLOW;
          matrixColor[4] = BLACK_ORANGE;
          matrixColor[5] = BLACK_GREEN;
          matrixColor[6] = BLACK;
          
          //return new MatrixToImageConfig(QrCode.Colors.WHITE.getArgb(), QrCode.Colors.ORANGE.getArgb());
          //return new MatrixToImageConfig(BLACK,WHITE);
          Random r = new Random();
          int colorRef = r.nextInt(matrixColor.length);  // Entre 0 y 5, más 1.
          return matrixColor[colorRef];
    }
    
    public static int getColorDot(int color){
        int[] matrixColor = new int[7];
          matrixColor[0] = BLACK;
          matrixColor[1] = BLACK_BLUE;
          matrixColor[2] = BLACK_RED;
          matrixColor[3] = BLACK_YELLOW;
          matrixColor[4] = BLACK_ORANGE;
          matrixColor[5] = BLACK_GREEN;
          matrixColor[6] = BLACK;
          return matrixColor[color];
    }
    
      private static MatrixToImageConfig getMatrixConfig() {
          return new MatrixToImageConfig(getColorDot(0),WHITE);
      }

      private String generateRandoTitle(Random random, int length) {
          return random.ints(48, 122)
                  .filter(i -> (i < 57 || i > 65) && (i < 90 || i > 97))
                  .mapToObj(i -> (char) i)
                  .limit(length)
                  .collect(StringBuilder::new, StringBuilder::append, StringBuilder::append)
                  .toString();
      }

      public enum Colors {

          BLUE(0xFF40BAD0),
          RED(0xFFE91C43),
          PURPLE(0xFF8A4F9E),
          ORANGE(0xFFF4B13D),
          WHITE(0xFFFFFFFF),
          BLACK(0xFF000000);

          private final int argb;

          Colors(final int argb){
              this.argb = argb;
          }

          public int getArgb(){
              return argb;
          }
      }
      
      public static String calcMaxChars(String text, int width,FontMetrics fm){
          int index=text.length();
          int lastIndex=0;
          int indexMax=text.length();
          do{
              String textAux = text.substring(0, index);
              int lenMax = fm.stringWidth(textAux);
              if (lenMax > width) {
                  indexMax = index;
                  
                  index -= (indexMax-lastIndex) / 2;
              }
              
              if (fm.stringWidth(textAux) < width) {
                  lastIndex=index;
                  index += (indexMax-lastIndex) / 2;
                  
              }
              if((indexMax-lastIndex)<=1)
                break;
          } while (index > 0);
   
       return text.substring(0,index);
      }
    public static void drawCenteredString(String text, int width, int posY, Graphics g) {
        FontMetrics fm = g.getFontMetrics();
        
        if (fm.stringWidth(text) < width) {
            int x = (width - fm.stringWidth(text)) / 2;
            g.drawString(text, x, posY);
        } else {
            String res = calcMaxChars(text,width,fm);

            int x = (width - fm.stringWidth(res)) / 2;
            g.drawString(res, x, posY);
        }

    
        
    }
      public static File addStringsImage(File fileOriginal,int pos,String pathDest, String... headers){
      File file=null;
      int spaces =0;
        try {
            BufferedImage imgOriginal = getOverly(fileOriginal);
            int width = imgOriginal.getWidth();
            int height = imgOriginal.getHeight();
            int sizeText=0;
            spaces = height/10;
            //calculate height append image height
            for(String line: headers){
                sizeText+=spaces;
                height+=spaces;
            }
            
            // Constructs a BufferedImage of one of the predefined image types.
            BufferedImage bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
            
            // Create a graphics which can be used to draw into the buffered image
            Graphics g2d = bufferedImage.createGraphics();
            
            // fill all the image with white
            g2d.setColor(Color.white);
            g2d.fillRect(0, 0, width, height);
            
            //set positions of each component
            int posText;
            int posImage;
            if(pos==0){
                posText =spaces;
                posImage = sizeText;
            }
            else{
                posImage = 0;
                posText = imgOriginal.getHeight()+spaces;
            }
            
            // add original image
            g2d.drawImage(imgOriginal, 0, posImage, null);
            
            for (String line : headers) {
                // create a string with black
                g2d.setColor(Color.black);
                int sizeFont = 14*height/270;
                g2d.setFont(new Font("TimesRoman", Font.PLAIN, sizeFont));
                g2d.setColor(Color.black);
                drawCenteredString(line, width, posText, g2d);
                posText += spaces;

            }



            // Disposes of this graphics context and releases any system resources that it is using.
            g2d.dispose();
            
            // Save as PNG
            file = new File(pathDest);
            ImageIO.write(bufferedImage, "png", file);
            
        } catch (IOException ex) {
            Logger.getLogger(GeneradorQR.class.getName()).log(Level.SEVERE, null, ex);
        }
 
    return file;
      }
      
  }
  