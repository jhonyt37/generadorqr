
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

public class PlainQR {

    private final static int MAX_HEIGHT_QR = 1024;
    private final static int MAX_LEN_QR = 512*MAX_HEIGHT_QR;
    
    public static void generateQR( byte []input, int len , int factor,String path) {
        byte [] buf = new byte[MAX_LEN_QR];
        BufferedImage img = mapSpaces( len, MAX_HEIGHT_QR,input, factor,5 );
        savePNG( img, path);
    }

    private static byte getBit(byte ID, int position) {
        return (byte) ((ID >> position) & 1);
    }
    
    private static void paintDot(BufferedImage res, int x, int y, int size, int color){
        int xTemp;
        int yTemp;
        for (yTemp = y; yTemp < (size + y); yTemp++) {
            for (xTemp = x; xTemp < (size + x); xTemp++) {
                //System.out.println(" xdot: " + xTemp + " ydot: " + yTemp);
                res.setRGB(xTemp, yTemp, color);
            }
        }
        
        
        
    }
    
    private static BufferedImage paintBoard(int sizeXaux, int sizeYaux,int factor){
         final BufferedImage res = new BufferedImage(sizeXaux, sizeYaux, BufferedImage.TYPE_INT_RGB);
        
        Graphics2D ig2 = res.createGraphics();
        ig2.setBackground(Color.WHITE);
        ig2.clearRect(0, 0, sizeXaux, sizeYaux);
        
        /*int auxX = 0;
        int auxY = 0;
        for (auxY = 0; auxY < sizeYaux; auxY += factor) {
            for (auxX = 0; auxX < sizeXaux; auxX += factor) {
                //System.out.println(" x: " + auxX + " y: " + auxY);
                paintDot(res, auxX, auxY, factor, Color.WHITE.getRGB());

            }
        }*/
        return res;
    }
    
    private static BufferedImage mapSpaces(int sizeX, int sizeY, byte[] buff, int factor, int spaces) {
        int height = (buff.length * 8 / sizeX);

        int posIniX = spaces * factor;
        int posIniY = spaces * factor;
        int posX = posIniX;
        int posY = posIniY;
        int posByte;

        int sizeYBoard = ((sizeX) * factor) + (spaces * 2 * factor);
        int sizeXBoard = ((sizeX) * factor) + (spaces * 2 * factor);
        final BufferedImage res = paintBoard(sizeXBoard, sizeYBoard, factor);

        try {
            for (posByte = 0; posByte < buff.length; posByte++) {
                for (int k = 7; k >= 0; k--) {
                    //System.out.println("Main.map() "+posByte +" "+ posX+ " " +posY);
                    byte val = getBit(buff[posByte], k);

                    if (val > 0) {
                        paintDot(res, posX, posY, factor, Color.WHITE.getRGB());
                        posX += factor;

                    } else {
                        //int divsion = (height*factor)/5;
                        //int colorRef = GeneradorQR.getColorDot(posY/divsion);
                        int colorRef = GeneradorQR.getColorDot(0);
                        paintDot(res, posX, posY, factor, colorRef);
                        posX += factor;
                    }
                    if (posX >= ((sizeX) * factor) + (spaces * factor)) {
                        posX = posIniX;
                        posY += factor;
                        if (posY >= ((height * factor) + (spaces * factor))) {
                            break;
                        }
                    }
                }
            }
            //System.out.println("Main.map()");
        } catch (Exception ex) {
            System.out.println("Main.map()" + ex.getMessage());
        }
        return res;
    }
    
    
    private static void savePNG( final BufferedImage bi, final String path ){
        try {
            RenderedImage rendImage = bi;
            File f = new File(path);
            ImageIO.write(rendImage, "png", f);
        } catch ( IOException e) {
            System.out.println("Main.savePNG()" + e.getMessage());
        }
    }

}